# RISK MAP #

Este projeto tem como foco o desenvolvimento de uma aplicativo na plataforma Android para  o mapeamento de riscos. Mais especificamente, se trata de uma ferramenta que faz o mapeamento dos riscos (ambientais, ergonômicos), através de fotos e a sua respecitiva geolocalização. Além da foto, existem alguns atributos associados aos riscos como “graú de risco”( a chance do risco ocorrer: alto, médio, baixo), e que são associadas a uma determinada geolocalização.
