package com.br.android.riskmap;

import java.util.ArrayList;

import com.br.android.riskmap.R;

import android.support.v4.app.*;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class RiskListFragment extends ListFragment {
	
	private static final String TAG = "RiskListFragment";
	private static final int REQUEST_RISK = 0;
	private ArrayList<Risk> riskRep;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
		
		getActivity().setTitle(R.string.risks_title);
		riskRep = RiskDB.getInstance(getActivity()).getRisks();
		
		RiskAdapter adapter = new RiskAdapter(riskRep);
		setListAdapter(adapter);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup parent,
		Bundle savedInstanceState) {
		View v = super.onCreateView(inflater, parent, savedInstanceState);

		ListView listView = (ListView)v.findViewById(android.R.id.list);
		registerForContextMenu(listView);
		
		return v;
	}
	
	private class RiskAdapter extends ArrayAdapter<Risk> {
		
		public RiskAdapter(ArrayList<Risk> risks) {
			super(getActivity(), 0, risks);
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				convertView = getActivity().getLayoutInflater()
				.inflate(R.layout.list_item_risk, null);
			}
			// Configure the view for this Crime
			Risk r = getItem(position);
			TextView titleTextView =
			(TextView)convertView.findViewById(R.id.risk_list_item_titleTextView);
			titleTextView.setText(r.getTitle());
			TextView locTextView =
			(TextView)convertView.findViewById(R.id.risk_list_item_locationTextView);
			locTextView.setText(r.getLocation());

			return convertView;
		}
	}
	
	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		Risk r = (Risk)(getListAdapter()).getItem(position);
		
		Intent intent = new Intent(getActivity(), RiskActivity.class);
		intent.putExtra(RiskFragment.EXTRA_RISK_ID, r.getId());
		startActivity(intent);
	}
	
	@Override
	public void onResume() {
		super.onResume();
		((RiskAdapter)getListAdapter()).notifyDataSetChanged();
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.risk_menu_list, menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				if (NavUtils.getParentActivityName(getActivity()) != null) {
					NavUtils.navigateUpFromSameTask(getActivity());
				}
				return true;
			case R.id.menu_item_new_risk:
				Risk r = new Risk();
				RiskDB.getInstance(getActivity()).addRisk(r);
				Intent i = new Intent(getActivity(), RiskActivity.class);
				i.putExtra(RiskFragment.EXTRA_RISK_ID, r.getId());
				startActivityForResult(i, 0);
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		getActivity().getMenuInflater().inflate(R.menu.risk_list_item_context, menu);
	}
	
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterContextMenuInfo info = (AdapterContextMenuInfo)item.getMenuInfo();
		int position = info.position;
		RiskAdapter adapter = (RiskAdapter)getListAdapter();
		Risk r = adapter.getItem(position);
		
		switch (item.getItemId()) {
			case R.id.menu_item_delete_risk:
			RiskDB.getInstance(getActivity()).deleteRisk(r);
			adapter.notifyDataSetChanged();
			return true;
		}
		return super.onContextItemSelected(item);
	}
	
}