package com.br.android.riskmap;

import java.util.UUID;

public class Risk {
	
    private UUID Id;
    private String Title;
    private String Location;
    private String Priority;
    
    public Risk() {
        Id = UUID.randomUUID();
    }

	public String getTitle() {
		return Title;
	}

	public void setTitle(String title) {
		Title = title;
	}

	public UUID getId() {
		return Id;
	}
	
    public String getLocation() {
		return Location;
	}

	public void setLocation(String location) {
		Location = location;
	}
	
	@Override
	public String toString() {
		return Title;
	}

	public String getPriority() {
		return Priority;
	}

	public void setPriority(String priority) {
		Priority = priority;
	}
}
