package com.br.android.riskmap;

import java.util.UUID;

import com.br.android.riskmap.R;

import android.support.v4.app.*;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class RiskFragment extends Fragment {
    public static final String EXTRA_RISK_ID = 
    		"com.br.android.riskmap.risk_id";
	private static final String RISK_OBJ = 
			"com.br.android.riskmap.risk_obj";
	Risk _Risk;
    Button _btnSaveField;
    EditText _TitleField;
    EditText _LocationField;
    Spinner _SpinnerPriority;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        _Risk = new Risk();
        
        UUID riskId = (UUID)getActivity().getIntent()
        		.getSerializableExtra(EXTRA_RISK_ID);
        _Risk = RiskDB.getInstance(getActivity()).getRisk(riskId);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_risk, parent, false);
        
        _TitleField = (EditText)v.findViewById(R.id.risk_title);
        _TitleField.setText(_Risk.getTitle()); 
        
        _LocationField = (EditText)v.findViewById(R.id.location_risk);
        _LocationField.setText(_Risk.getLocation());
        
        _SpinnerPriority = (Spinner) v.findViewById(R.id.spinner_priority_risk);
        ArrayAdapter<CharSequence> adapterPriority = ArrayAdapter.createFromResource(getActivity(),
				R.array.priority_array_values, android.R.layout.simple_dropdown_item_1line);
        
        _SpinnerPriority.setAdapter(adapterPriority);
        _SpinnerPriority.setSelection(adapterPriority.getPosition(_Risk.getPriority()));
        
        _btnSaveField = (Button)v.findViewById(R.id.btn_save);
        _btnSaveField.setOnClickListener(new OnClickListener() {
			
			public void onClick(View arg0) {
				_Risk.setTitle(_TitleField.getText().toString());
				_Risk.setLocation(_LocationField.getText().toString());
				_Risk.setPriority((String)_SpinnerPriority.getSelectedItem());
				Toast.makeText(getActivity(), "Salvo com sucesso!", Toast.LENGTH_SHORT).show();
				getActivity().finish();
			}
		});
        
        return v; 
    }
    
}
