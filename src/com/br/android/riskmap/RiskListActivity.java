package com.br.android.riskmap;

import android.support.v4.app.Fragment;

public class RiskListActivity extends SingleFragmentActivity {

	@Override
	protected Fragment createFragment() {
		return new RiskListFragment();
	}

}
