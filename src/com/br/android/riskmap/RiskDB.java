package com.br.android.riskmap;

import java.util.ArrayList;
import java.util.UUID;

import android.content.Context;

public class RiskDB {
	private static RiskDB sRiskRep;
	private Context AppContext;
	private ArrayList<Risk> riskRep;
	
	private RiskDB(Context appContext) {
		AppContext = appContext;
		riskRep = new ArrayList<Risk>();
		
		Risk r1 = new Risk();
		r1.setTitle("Risco de Incêndio");
		r1.setLocation("RU - UFRPE");
		riskRep.add(r1);
		
		Risk r2 = new Risk();
		r2.setTitle("Vazamento de água");
		r2.setLocation("Prédio Central");
		riskRep.add(r2);
		
		Risk r3 = new Risk();
		r3.setTitle("Goteira - Risco de queimar equipamento");
		r3.setLocation("Prédio Central");
		riskRep.add(r3);
		
	}
	
	public static RiskDB getInstance(Context c) {
		if (sRiskRep == null) {
			sRiskRep = new RiskDB(c.getApplicationContext());
		}
		return sRiskRep;
	}
	
	public ArrayList<Risk> getRisks() {
		return riskRep;
	}
	
	public void addRisk(Risk c) {
		riskRep.add(c);
	}
	
	public void deleteRisk(Risk r) {
		riskRep.remove(r);
	}
	
	public Risk getRisk(UUID id) {
		for (Risk r : riskRep) {
			if (r.getId().equals(id))
				return r;
		}
		return null;
	}
}