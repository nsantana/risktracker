package com.br.android.riskmap;

import com.br.android.riskmap.R;
import android.os.Bundle;
import android.support.v4.app.*;

public class RiskActivity extends SingleFragmentActivity {

	@Override
	protected Fragment createFragment() {
		return new RiskFragment();
	}

}
